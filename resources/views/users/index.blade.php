@extends('layouts.app')

@section('title', 'Psychologists Clinic')

@section('content')

<h1>List of users</h1>

<table class = "table table-dark">
    <tr>
        <th>id</th>
        <th>Name</th>
        <th>Role</th>
        @if (auth()->check())
            @if (auth()->user()->isAdmin())
            <th>change role</th>
            @endif
        @endif    
        @can ('edit-user')
            <th>edit</th>
        @endcan
    </tr>
    <!-- the table data -->
    @foreach($users as $user)
            <td>{{$user->id}}</td>
            <td>{{$user->name}}</td>
            <td>
                {{$user->roles->name}} 
            </td>
            @if (auth()->check())
                @if (auth()->user()->isAdmin())
                <td>
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @if(isset($user->role_id))
                          {{$user->roles->name}}
                        @else
                          Change role  
                        @endif
                     </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    @foreach($roles as $role)
                      <a class="dropdown-item" href="{{route('users.changerole',[$user->id,$role->id])}}">{{$role->name}}</a>
                    @endforeach
                    </div>
                </div>                
            </td>  
                @endif
            @endif
            @can ('edit-user')
            <td><a href = "{{route('users.edit',$user->id)}}">Edit</a></td>
            @endcan
        </tr>
    @endforeach
</table>
@endsection

